import socket
import picar_4wd as fc
import os
import RPi.GPIO as GPIO
import time, math
import threading

HOST = "172.20.10.10" # IP address of your Raspberry PI
PORT = 65433          # Port to listen on (non-privileged ports are > 1023)

# Adapted from picar_4wd speed.py library
class Speed():
    def __init__(self, pin):
        self.speed_counter = 0
        self.speed = 0
        self.last_time = 0
        self.pin = pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        self.timer_flag = True
        self.timer = threading.Thread(target=self.fun_timer, name="Thread1")

    def start(self):
        self.timer.start()
        # print('speed start')

    def fun_timer(self):
        while self.timer_flag:
            l = ""
            for _ in range(100):
                l += str(GPIO.input(self.pin))
                time.sleep(0.001)
            # self.print_result(l)
            count = (l.count("01") + l.count("10")) / 2
            rps = count / 20.0 * 10
            self.speed = round(2 * math.pi * 3.3 * rps, 2)

    def __call__(self):
        return self.speed

    def deinit(self):
        self.timer_flag = False
        self.timer.join()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    speed4 = Speed(4) 
    speed4.start()
    try:
        while 1:
            client, clientInfo = s.accept()
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            # "temp,voltage,ultrasonic"
            tmp = os.popen("../../../usr/bin/vcgencmd measure_temp").read()
            ultrasonic = fc.get_distance_at(0)
            car_data = "{},{},{}".format(tmp,ultrasonic,speed4()).encode()
            client.sendall(car_data) # Echo back to client

            if data != b"":
                print(data.rstrip())
                if(data.rstrip() == b"87"): # forward
                    fc.forward(50)
                elif(data.rstrip() == b"83"): # backward
                    fc.forward(-50)
                elif(data.rstrip() == b"65"): # left
                    fc.turn_left(50)
                elif(data.rstrip() == b"68"): # right
                    fc.turn_right(50)
                else:
                    fc.stop()
            
    except Exception as e: 
        print("Closing socket")
        speed4.deinit()
        fc.stop()
        client.close()
        s.close()
